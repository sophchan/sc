# Sophchan
**Sophchan** is an open-source message-board suite for your enjoyment. It doesn't do images, and probably never will implement that functionality, but who needs pictures in Web 1.0? Not Sophchan!

## Using Sophchan
So you want to use this god-awful software? Good for you! First, since we're *kinda bad at programming*, you'll have to configure your `config.php` file manually. Be sure to change `$board_name`, etc. After you get through those simple settings, your board should be usable*.


*By "usable" we mean that it works as released, not that this software is even remotely good.


[![Shitty shill shvideo](https://files.catbox.moe/3astz8.png)](https://files.catbox.moe/f4dsr0.mp4 "Link Title")


Developed and maintained by [Honest](https://chadland.net/honest)
