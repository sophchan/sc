<?php
// initial stuff
include 'config.php';
$servername = $host;
$username = $user;
$password = $pass;
$dbase = $database;

// Create connection
$conn = new mysqli($servername, $username, $password, $dbase);
// Check connection
if ($conn->connect_error) {
  die("Connection failed: " . $conn->connect_error);
}
$sql = "SELECT motd FROM acp";
$result = $conn->query($sql);

if (!empty($result) && $result->num_rows > 0) {
  while($row = $result->fetch_assoc()) {
    global $curmotd;
    $curmotd = $row["motd"];
  }
} else {
  echo "404, MOTD not found [real error]";
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
  <link rel="stylesheet" href="global.css">
  <title><?php echo $board_name; ?></title>
  <meta charset="UTF-8" />
  <meta name="viewport" content="width=device-width,initial-scale=1" />
  <meta name="description" content="" />
  <script src="global.js"></script>
</head>
<body>

  <center>
  <h1>Welcome to <i><?php echo $board_name; ?></i>!</h1>
  <hr>
<form action="submit.php" method="post">
Subject: <input type="text" name="subject"><br>
Text: <input type="text" name="body"><br>
<input type="submit">
<label for="flag">Choose a flag:</label>
<select name="flag" id="flag">
  <option value="us">United States</option>
  <option value="si">Serbia</option>
  <option value="kek">Kekistan</option>
</select>
</form>
<h2 class="motd"><?php echo $curmotd;?></h2>
</center>
<hr>
<?php

$sql = "
SELECT id, post_subject, post_body, post_time, flag FROM sc ORDER BY id DESC";
$result = $conn->query($sql);

if ($result->num_rows > 0) {
  // output data of each row
  while($row = $result->fetch_assoc()) {
    echo "<div id='"  . $row["id"].  "'><div class='post'><i><a href='#"  . $row["id"].  "'>ID: " . $row["id"]. "</i></a> | <img src='flag/" . $row["flag"]. ".png'> | Subject: " . $row["post_subject"]. "<br>" . $row["post_body"]. "</div><br>";
  }
} else {
  echo "Error, shit fucked up. (Or the DB was cleared)";
}
$conn->close();
?>


<div class="btm-ftr">
  <input type="button" value="Show ACP" onclick="show_acp()" class="hidden-btn">
  <div id="admin-dd" class="admin-dd">
  Current motd:
  <?php
echo $curmotd;
  ?>
  <br>
  <form action="motd.php" method="post">
Set motd: <input type="text" name="setmotd">
<input type="submit" value="set!">
</form>
  </div>
</div>
</body>
</html>
