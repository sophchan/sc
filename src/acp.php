<?php
// initial stuff
include 'config.php';
$servername = $host;
$username = $user;
$password = $pass;
$dbase = $database;

// Create connection
$conn = new mysqli($servername, $username, $password, $dbase);
// Check connection
if ($conn->connect_error) {
  die("Connection failed: " . $conn->connect_error);
}
$sql = "SELECT motd FROM acp";
$result = $conn->query($sql);

if (!empty($result) && $result->num_rows > 0) {
  while($row = $result->fetch_assoc()) {
    global $curmotd;
    $curmotd = $row["motd"];
  }
} else {
  echo "404, MOTD not found [real error]";
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
  <link rel="stylesheet" href="/sc/src/global.css">
  <title>ACP - <?php echo $board_name;?></title>
  <meta charset="UTF-8" />
  <meta name="viewport" content="width=device-width,initial-scale=1" />
  <meta name="description" content="" />
  <script src="/sc/src/global.js"></script>
</head>
<body>

  <center>
  <h1>Admin Control Panel - <?php echo $board_name;?></h1>
Control <?php echo $board_name;?> from this 'mastermind' page!
  <hr>
  <b>MOTD settings</b>
  <form class="post" action="motd.php" method="post">
    <?php
    echo "Current MOTD: ", $curmotd, "<br>";
    ?>
Set motd: <input type="text" name="setmotd">
<input type="submit" value="set!">
</form>
</body>
</html>
